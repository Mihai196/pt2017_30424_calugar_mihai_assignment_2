package mypackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Store extends Thread {
	private int minArrivTime;
	private int maxArrivTime;
	private int minServTime;
	private int maxServTime;
	private int nrofQueues;
	private int simTime;
	private int nrofClients;
	private static int realTime;
	private List<Client> clientlist = new ArrayList<Client>();
	private Random rand = new Random();
	private static Queue[] queuelist;
	private static String finalresults;
	private static int waitTime;
	private int totalemptyTime;
	private int totalServTime;
	private int peakTime;
	private int nrclientsPeak = 0;

	/*Constructor for the store class in which we instantiate an object of type store with the mentioned fields
	 * */
	public Store(int minArrivTime, int maxArrivTime, int minServTime, int maxServTime, int nrofQueues, int simTime) {
		this.minArrivTime = minArrivTime;
		this.maxArrivTime = maxArrivTime;
		this.minServTime = minServTime;
		this.maxServTime = maxServTime;
		this.nrofQueues = nrofQueues;
		this.simTime = simTime;
		this.nrofClients = 20;
		queuelist = new Queue[this.nrofQueues];
		for (int i = 0; i < nrofQueues; i++) {
			queuelist[i] = new Queue();
		}
	}
	
	public static String getResults() {
		return finalresults;
	}

	public static void addText(String toAdd) {
		finalresults += toAdd;
	}

	public static void setemptyLog() {
		finalresults = "";
	}

	public static int getrealTime() {
		return realTime;
	}

	public static void addWaitTime(int timetoAdd) {
		waitTime += timetoAdd;
	}
	/*Finds the optimal queue for a client to join based on the number of clients in the queue 
	 * */
	public int findQueue() {
		int indice = 0;
		int min = queuelist[0].getQueueDim();
		for (int i = 1; i < queuelist.length; i++) {
			if (queuelist[i].getQueueDim() < min){
				min=queuelist[i].getQueueDim();
				indice = i;
		}
		}
		return indice;
	}
	/*Computed peak time more specifically the number of clients at a certain time storing in peakTime the time when 
	 * the clients were the most and in nrclientsPeak the number of clients for that specific peakTime
	 * */
	public void computepeakTime() {
		int clientcnt = 0;
		for (int i = 0; i < queuelist.length; i++) {
			clientcnt += queuelist[i].getQueueDim();
		}
		if (clientcnt > nrclientsPeak) {
			nrclientsPeak = clientcnt;
			peakTime = realTime;
		}
	}
	/*Generates randomly an arrival time and a serving time and then adds the client to the field client list
	 * After that the we start the thread for queue in using this method in the thread for the store
	 * */
	public void setClients() {
		for (int i = 0; i < nrofClients; i++) {
			int arrivTime = minArrivTime + rand.nextInt(maxArrivTime - minArrivTime);
			int servTime = minServTime + rand.nextInt(maxServTime - minServTime);
			clientlist.add(new Client(i, arrivTime, servTime));
		}
		Store.addText("Simulation started: \n");
		for (Queue currqueue : queuelist) {
			currqueue.start();
		}
	}

	/*The run method which implements the thread for the store in which we use the methods described before to assign
	 * each client to a queue, find its arrival time and serving time and compute basically each time which is the peakTime
	 * In the end we compute the total empty time and set the results of the simulation 
	 * */
	public void run() {
		setClients();
		for (realTime = 0; realTime <= simTime; realTime++) {
			for (Client c : clientlist) {
				if (c.getArrivTime() == realTime) {
					int q = findQueue();
					queuelist[q].addClient(c);
					totalServTime += c.getServTime();
					Store.addText("Client: " + (c.getId() + 1) + " at queue: " + (q + 1) + " arrived at "
							+ (c.getArrivTime()) + " serving time " + c.getServTime() + "\n");
				}
			}
			computepeakTime();
			try {
				GUI.simlog.setText(Store.getResults());
				sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("Interrupted");
			}
		}
		for (int i = 0; i < queuelist.length; i++) {
			totalemptyTime += queuelist[i].getemptyTime();
		}
		setResults();
	}
	/*Set results simply computes the minimal output results and adds them to the field finalResults
	 * */
	public void setResults() {
		Store.addText("Total number of clients: " + nrofClients + "\n" + "Average service time: "
				+ ((float) totalServTime / nrofClients) + "\n" + "Average waiting time: "
				+ ((float) waitTime / nrofClients) + "\n" + "Average empty queue time: "
				+ ((float) totalemptyTime / queuelist.length) + "\n" + "Peak time: " + peakTime+ 
				"\nNumber of clients at peak time: " + nrclientsPeak);
		GUI.simlog.setText(Store.getResults());
	}

}
