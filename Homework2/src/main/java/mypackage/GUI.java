package mypackage;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class GUI {
	public JFrame frame;
	public static JTextArea simlog;
	private JTextField minarrivtime;
	private JTextField maxarrivtime;
	private JTextField minservtime;
	private JTextField maxservtime;
	private JTextField nrofqueues;
	private JTextField simtime;

	public GUI() {
		initframe();
		addLabels();
		addTextFields();
		addButtons();
	}

	public void initframe() {
		frame = new JFrame("Store simulation");
		frame.setBounds(10, 10, 600, 350);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
	}

	public void addLabels() {
		JLabel lbl1 = new JLabel("->Minimum Arrival Time: ");
		lbl1.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lbl1.setBounds(10, 10, 200, 20);
		frame.add(lbl1);

		JLabel lbl2 = new JLabel("->Maximum Arrival Time: ");
		lbl2.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lbl2.setBounds(10, 40, 200, 20);
		frame.add(lbl2);

		JLabel lbl3 = new JLabel("->Minimum Service Time: ");
		lbl3.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lbl3.setBounds(10, 70, 200, 20);
		frame.add(lbl3);

		JLabel lbl4 = new JLabel("->Maximum Service Time: ");
		lbl4.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lbl4.setBounds(10, 100, 200, 20);
		frame.add(lbl4);

		JLabel lbl5 = new JLabel("->Number of queues: ");
		lbl5.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lbl5.setBounds(10, 130, 200, 20);
		frame.add(lbl5);

		JLabel lbl6 = new JLabel("->Simulation time: ");
		lbl6.setFont(new Font("Times New Roman", Font.BOLD, 14));
		lbl6.setBounds(10, 160, 200, 20);
		frame.add(lbl6);

	}

	public void addTextFields() {
		minarrivtime = new JTextField();
		minarrivtime.setBounds(190, 10, 50, 20);
		frame.add(minarrivtime);

		maxarrivtime = new JTextField();
		maxarrivtime.setBounds(190, 40, 50, 20);
		frame.add(maxarrivtime);

		minservtime = new JTextField();
		minservtime.setBounds(190, 70, 50, 20);
		frame.add(minservtime);

		maxservtime = new JTextField();
		maxservtime.setBounds(190, 100, 50, 20);
		frame.add(maxservtime);

		nrofqueues = new JTextField();
		nrofqueues.setBounds(190, 130, 50, 20);
		frame.add(nrofqueues);

		simtime = new JTextField();
		simtime.setBounds(190, 160, 50, 20);
		frame.add(simtime);

		simlog = new JTextArea();
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(260, 10, 300, 275);
		scrollPane.setViewportView(simlog);
		frame.add(scrollPane);
	}

	public void addButtons() {
		JButton btn1 = new JButton("Start Simulation ");
		btn1.setBounds(10, 200, 220, 20);
		frame.add(btn1);

		btn1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Store.setemptyLog();
				queueSimulation();
			}
		});
		JButton btn2 = new JButton("Clear Results");
		btn2.setBounds(10, 240, 220, 20);
		frame.add(btn2);
		btn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Store.setemptyLog();
				GUI.simlog.setText("");
			}
		});

	}

	public void queueSimulation() {
		try {
			int x1 = (int) Integer.parseInt(minarrivtime.getText());
			int x2 = (int) Integer.parseInt(maxarrivtime.getText());
			int x3 = (int) Integer.parseInt(minservtime.getText());
			int x4 = (int) Integer.parseInt(maxservtime.getText());
			int x5 = (int) Integer.parseInt(nrofqueues.getText());
			int x6 = (int) Integer.parseInt(simtime.getText());
			if (x1 > x2)
				JOptionPane.showMessageDialog(null, "Exception: Min arrival time greater than max arrival time");
			else if (x3 > x4)
				JOptionPane.showMessageDialog(null, "Exception: Min serving time greater than max serving time");
			else if (x2 > x6)
				JOptionPane.showMessageDialog(null, "Exception: Arrival time greater than simulation time");
			else if (x4 > x6)
				JOptionPane.showMessageDialog(null, "Exception: Serving time greater than simulation time");
			else {
				Store store = new Store(x1, x2, x3, x4, x5, x6);
				store.start();
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "Exception: Please introduce data as positive integers, no spaces or other"
					+ " characters and no empty fields");
		}

	}
}
