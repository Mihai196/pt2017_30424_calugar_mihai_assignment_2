package mypackage;

import java.util.LinkedList;

public class Queue extends Thread {
	private LinkedList<Client> clientqueue = new LinkedList<Client>();
	private int emptyTime;

	public int getQueueDim() {
		return clientqueue.size();
	}

	public int getemptyTime() {
		return emptyTime;
	}

	public void addClient(Client client) {
		clientqueue.add(client);
	}

	/* Thread for queue: sleeps for servTime seconds in which the first customer is served computes the leaving time
	 * and finally removes the client from the queue 
	*/
	public void run() {
		while (true) {
			if (clientqueue.isEmpty() == false) {
				try {
					sleep(clientqueue.getFirst().getServTime() * 1000);
					Store.addText("Client: " + (clientqueue.getFirst().getId() + 1) + " Leaving time : "
							+ (Store.getrealTime() + 1) + "\n");
				} catch (InterruptedException e) {
					System.out.println("Interrupted");
				} finally {
					Store.addWaitTime(Store.getrealTime() - clientqueue.getFirst().getServTime()-clientqueue.getFirst().getArrivTime());
					clientqueue.removeFirst();
				}
			} else {
				emptyTime++;
				try {
					sleep(1000);
				} catch (InterruptedException e) {
					System.out.println("Interrupted");
				}
			}
		}
	}
}
