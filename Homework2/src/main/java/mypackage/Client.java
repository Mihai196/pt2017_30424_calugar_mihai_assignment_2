package mypackage;

public class Client {
	private int id;
	private int arrivTime;
	private int servTime;

	public Client(int id, int arrivTime, int servTime) {
		this.id = id;
		this.arrivTime = arrivTime;
		this.servTime = servTime;
	}

	public int getId() {
		return id;
	}

	public int getArrivTime() {
		return arrivTime;
	}

	public int getServTime() {
		return servTime;
	}
}
